﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.InteropServices;

namespace ACCESO
{
    public class Acceso
    {
        private SqlConnection cn;

        public void Abrir()
        {
            cn = new SqlConnection("Data Source=.\\Sqlexpress; Initial Catalog= ParcialEmpleadoRecibo; Integrated Security=SSPI");
            cn.Open();
        }

        public void Cerrar()
        {
            cn.Close();
            cn = null;
            GC.Collect();
        }

        private SqlCommand CrearComandoSin(string Sql)
        {
            SqlCommand comando = new SqlCommand(Sql, cn);
            return comando;
        }

        private SqlCommand CrearComando(string Sql, List<IDbDataParameter> parametros = null, CommandType tipo = CommandType.StoredProcedure)
        {
            SqlCommand comando = new SqlCommand(Sql, cn);
            comando.CommandType = tipo;
            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }
            return comando;
        }

        public int Escribir(string Sql, List<IDbDataParameter> parametros = null)
        {
            SqlCommand cmd = CrearComando(Sql, parametros);
            int resultado;
            try
            {
                resultado = cmd.ExecuteNonQuery();
            }
            catch
            {
                resultado = -1;
            }

            return resultado;
        }

        public DataTable Reporte(string query)
        {
            SqlDataAdapter ad = new SqlDataAdapter();
            ad.SelectCommand = CrearComandoSin(query);
            DataTable tabla = new DataTable();
            ad.Fill(tabla);

            return tabla;
        }

        public DataTable Leer(string Sql, List<IDbDataParameter> parametros = null)
        {
            SqlDataAdapter ad = new SqlDataAdapter();
            ad.SelectCommand = CrearComando(Sql, parametros);
            DataTable tabla = new DataTable();
            ad.Fill(tabla);

            return tabla;
        }

        public IDbDataParameter CrearParametro(string nom, string valor)
        {
            SqlParameter par = new SqlParameter(nom, valor);
            par.DbType = DbType.String;
            return par;
        }

        public IDbDataParameter CrearParametro(string nom, int valor)
        {
            SqlParameter par = new SqlParameter(nom, valor);
            par.DbType = DbType.Int32;
            return par;
        }

        public IDbDataParameter CrearParametro(string nom, DateTime valor)
        {
            SqlParameter par = new SqlParameter(nom, valor);
            par.DbType = DbType.DateTime;
            return par;
        }

        public IDbDataParameter CrearParametro(string nom, bool valor)
        {
            SqlParameter par = new SqlParameter(nom, valor);
            par.DbType = DbType.Boolean;
            return par;
        }

        public IDbDataParameter CrearParametro(string nom, double valor)
        {
            SqlParameter par = new SqlParameter(nom, valor);
            par.DbType = DbType.Double;
            return par;
        }
    }
}
