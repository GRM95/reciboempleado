﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NEGOCIO;



namespace PRESENTACION
{

    public partial class Inicio : Form
    {
        public Inicio()
        {
            InitializeComponent();
        }

        public AltaEmpleado FormAltaEmpleado;
        public ModificacionEmpleado FormModificacionEmpleado;
        public BajaEmpleado FormBajaEmpleado;

        public AltaConcepto FormAltaConcepto;
        public ModificacionConcepto FormModificacionConcepto;
        public BajaConcepto FormBajaConcepto;

        public AltaRecibo FormAltaRecibo;

        public Empleado empleado = new Empleado();
        public Concepto concepto = new Concepto();




        private void Form1_Load(object sender, EventArgs e)
        {

        }



        private void Enlazar()
        {
            dataGridView2.DataSource = null;
            dataGridView2.DataSource = Recibo.Listar();
        }



        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void altaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAltaEmpleado = new AltaEmpleado();
            FormAltaEmpleado.cargarEmpleados(empleado);
            FormAltaEmpleado.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        { 

        }

        private void bajaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormModificacionEmpleado = new ModificacionEmpleado();
            FormModificacionEmpleado.cargarEmpleados(empleado);
            FormModificacionEmpleado.Show();
        }

        private void groupBox1_Enter_1(object sender, EventArgs e)
        {

        }

        private void reciboToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void bajaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
                FormBajaEmpleado = new BajaEmpleado();
                FormBajaEmpleado.cargarEmpleados(empleado);
                FormBajaEmpleado.Show();
        }

        private void altaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FormAltaConcepto = new AltaConcepto();
            FormAltaConcepto.cargarConceptos(concepto);
            FormAltaConcepto.Show();
        }

        private void modificacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormModificacionConcepto = new ModificacionConcepto();
            FormModificacionConcepto.cargarConceptos(concepto);
            FormModificacionConcepto.Show();
        }

        private void bajaToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            FormBajaConcepto = new BajaConcepto();
            FormBajaConcepto.cargarConceptos(concepto);
            FormBajaConcepto.Show();
        }

        private void altaToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            FormAltaRecibo = new AltaRecibo();
            FormAltaRecibo.cargarConceptos(concepto);
            FormAltaRecibo.cargarEmpleados(empleado);
            FormAltaRecibo.Show();
        }

        private void listarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FormAltaRecibo = new AltaRecibo();
            FormAltaRecibo.cargarConceptos(concepto);
            FormAltaRecibo.cargarEmpleados(empleado);
            FormAltaRecibo.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Empleado.Listar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                dataGridView2.DataSource = null;

                Reporte a = new Reporte();
                dataGridView2.DataSource = Reporte.Listar(idGrid);
                /*a.InsertarQuery(idGrid);*/

                
            }
            else
            {
                MessageBox.Show("Seleccione un empleado");
            }

        }

        public int idGrid = 0;
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            idGrid = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void listarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
