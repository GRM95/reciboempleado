﻿using NEGOCIO;
using System;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class AltaEmpleado : Form
    {
        public AltaEmpleado()
        {
            InitializeComponent();
        }

        public Empleado empleado;
        public void cargarEmpleados(Empleado empleado)
        {
            this.empleado = empleado;
        }

        private void AltaEmpleado_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text) && string.IsNullOrEmpty(textBox2.Text) && string.IsNullOrEmpty(textBox3.Text))
            {
                MessageBox.Show("Ingresar datos");
            }
            else
            {
                Empleado p = new Empleado();
                p.Nombre = textBox1.Text;
                p.Apellido = textBox2.Text;
                p.Cuil = int.Parse(textBox3.Text);
                p.FechaAlta = DateTime.Today;


                p.Insertar();

                MessageBox.Show("Empleado dado de alta");
                textBox1.Clear();
                textBox2.Clear();
                textBox3.Clear();
            }

        }


        IValidar objeto = new VALIDACIONES();



        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            objeto.ValidarLetras(e);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            objeto.ValidarLetras(e);
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            objeto.ValidarNumeros(e);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
