﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NEGOCIO;

namespace PRESENTACION
{
    public partial class ModificacionConcepto : Form
    {
        public ModificacionConcepto()
        {
            InitializeComponent();
        }


        public Concepto concepto;
        public void cargarConceptos(Concepto concepto)
        {
            this.concepto = concepto;
        }

        private void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Concepto.Listar();
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void ModificacionConcepto_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text) && string.IsNullOrEmpty(textBox2.Text) && string.IsNullOrEmpty(textBox3.Text) && string.IsNullOrEmpty(textBox4.Text))
            {
                MessageBox.Show("ingrese inormacion");
            }
            else
            {

                bool temporal;
                if (comboBox1.Text == "Positivo")
                {
                    temporal = true;
                }
                else
                {
                    temporal = false;
                }
                Concepto p = new Concepto(int.Parse(textBox1.Text), textBox2.Text, textBox3.Text, temporal, float.Parse(textBox4.Text));
                p.Editar();

                Enlazar();
            }

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBox3.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();

            if (dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString() == "True")
            {
                comboBox1.Text = "Positivo";
            }
            else
            {
                comboBox1.Text = "Negativo";

            }
            textBox4.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
        }

        IValidar objeto = new VALIDACIONES();

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            objeto.ValidarNumeros(e);
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            objeto.ValidarLetras(e);
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            objeto.ValidarLetras(e);
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            objeto.ValidarNumeros(e);
        }
    }
}
