﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlTypes;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NEGOCIO;


namespace PRESENTACION
{
    public partial class AltaRecibo : Form
    {
        public AltaRecibo()
        {
            InitializeComponent();
        }

        public Empleado empleado;
        public void cargarEmpleados(Empleado empleado)
        {
            this.empleado = empleado;
        }

        public Concepto concepto;
        public void cargarConceptos(Concepto concepto)
        {
            this.concepto = concepto;
        }



        private void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView2.DataSource = null;
            dataGridView1.DataSource = Concepto.Listar();
            dataGridView2.DataSource = Empleado.Listar();
        }




        private void AltaRecibo_Load(object sender, EventArgs e)
        {
            Enlazar();
            label11.Text = null;
            label15.Text = null;
            label7.Text = null;
            label8.Text = null;
            label9.Text = null;
            label10.Text = null;
            label18.Text = null;
            label22.Text = null;
            label20.Text = null;
            label14.Text = null;

        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            label15.Text = dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString();
            label7.Text = dataGridView2.Rows[e.RowIndex].Cells[1].Value.ToString();
            label8.Text = dataGridView2.Rows[e.RowIndex].Cells[2].Value.ToString();
            label9.Text = dataGridView2.Rows[e.RowIndex].Cells[3].Value.ToString();
            label10.Text = dataGridView2.Rows[e.RowIndex].Cells[4].Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(textBox1.Text) || string.IsNullOrWhiteSpace(label20.Text))
            {
                MessageBox.Show("Ingresar sueldo bruto y seleccionar concepto");
            }
            else
            {
                listBox1.Items.Add(label22.Text + ',' + label20.Text + ',' + label24.Text);
            }
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            label24.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            label18.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            label20.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();

            if (label20.Text == "True")
            {
                label20.Text = "Positivo";
            }
            else
            {
                label20.Text = "Negativo";
            }

            label22.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        public float temporal = 0;


        private void button3_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text))
            {
                MessageBox.Show("Ingrese sueldo bruto");
            }
            else
            {
                if (listBox1.Items.Count == 0)
                {
                    MessageBox.Show("Seleccione conceptos para el recibo");
                }
                else
                {
                    foreach (var item in listBox1.Items)
                    {
                        string[] arreglo = item.ToString().Split(',');

                        if (arreglo[1] == "Positivo")
                        {
                            temporal = (float.Parse(textBox1.Text) * float.Parse(arreglo[0].ToString()) / 100) + temporal;
                        }
                        else
                        {
                            temporal = temporal - (float.Parse(textBox1.Text) * float.Parse(arreglo[0].ToString()) / 100);
                        }


                    }
                    label11.Text += (temporal * -1).ToString(); ;

                    float totalBruto = float.Parse(textBox1.Text);
                    totalBruto = temporal + totalBruto;
                    label14.Text = totalBruto.ToString();
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (listBox1.Items.Count == 0)
            {
                MessageBox.Show("No hay conceptos seleccionados");
            }
            else
            {
                listBox1.Items.RemoveAt(listBox1.Items.Count - 1);
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(label15.Text))
            {
                MessageBox.Show("Debe seleccionar un empleado");
            }
            else
            {
                if (string.IsNullOrEmpty(textBox1.Text) && string.IsNullOrEmpty(listBox1.Text))
                {
                    MessageBox.Show("Minimo debe seleccionar un empleado y un concepto.");
                }
                else
                {
                    if (string.IsNullOrEmpty(label14.Text))
                    {
                        MessageBox.Show("Debe tener calculado un sueldo neto.");

                    }
                    else
                    {
                        Recibo p = new Recibo();
                        p.Mes = int.Parse(DateTime.Now.Month.ToString());
                        p.Ano = int.Parse(DateTime.Now.Year.ToString());
                        p.SueldoBruto = float.Parse(textBox1.Text);
                        p.SueldoNeto = float.Parse(label14.Text);
                        p.TotalDescuento = float.Parse(label11.Text);

                        p.Insertar();

                        foreach (var item in listBox1.Items)
                        {
                            string[] arreglo1 = item.ToString().Split(',');
                            Intermediador i = new Intermediador();
                            i.IdConcepto = int.Parse(arreglo1[2].ToString());

                            i.InsertarReciboConcepto();
                        }

                        

                        Empleado h = new Empleado();
                        h.Legajo = int.Parse(label15.Text);
                        h.InsertarReciboLegajo();
                        

                        

                        MessageBox.Show("Recibo dado de alta");
                        label11.Text = null;
                        label15.Text = null;
                        label7.Text = null;
                        label8.Text = null;
                        label9.Text = null;
                        label10.Text = null;
                        label18.Text = null;
                        label22.Text = null;
                        label20.Text = null;
                        label14.Text = null;
                        textBox1.Text = null;
                        listBox1.Items.Clear();
                        textBox1.Clear();
                    }
                }
            }
        }

        IValidar objeto = new VALIDACIONES();


        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            objeto.ValidarNumeros(e);
        }
    }
}
