﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NEGOCIO;

namespace PRESENTACION
{
    public partial class BajaEmpleado : Form
    {
        public BajaEmpleado()
        {
            InitializeComponent();
        }

        public Empleado empleado;
        public void cargarEmpleados(Empleado empleado)
        {
            this.empleado = empleado;
        }

        private void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Empleado.Listar();

        }

        private void BajaEmpleado_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label6.Text = null;
            label7.Text = null;
            label8.Text = null;
            label9.Text = null;
            label10.Text = null;
            try
            {
                Empleado p = new Empleado(int.Parse(label6.Text));
                p.Borrar();

                Enlazar();
            }
            catch (Exception)
            {
                MessageBox.Show("Seleccione un empleado");
            }

            label6.Text = null;
            label7.Text = null;
            label8.Text = null;
            label9.Text = null;
            label10.Text = null;

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            label6.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            label7.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            label8.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            label9.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            label10.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();

        }
    }
}
