﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NEGOCIO;


namespace PRESENTACION
{
    public partial class ModificacionEmpleado : Form
    {
        public ModificacionEmpleado()
        {
            InitializeComponent();
        }


        public Empleado empleado;
        public void cargarEmpleados(Empleado empleado)
        {
            this.empleado = empleado;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void ModificacionEmpleado_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Empleado.Listar();

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBox3.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            textBox4.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text) && string.IsNullOrEmpty(textBox2.Text) && string.IsNullOrEmpty(textBox3.Text) && string.IsNullOrEmpty(textBox4.Text))
            {
                MessageBox.Show("Ingrese datos");
            }
            else
            {
                Enlazar();
                if (dataGridView1.Rows.Count <= 0 )
                {
                    MessageBox.Show("No hay empleados");
                }
                else
                {

                    Empleado p = new Empleado(int.Parse(textBox1.Text), textBox2.Text, textBox3.Text, int.Parse(textBox4.Text));
                    p.Editar();

                    Enlazar();
                }
            }


        }

        IValidar objeto = new VALIDACIONES();


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            objeto.ValidarNumeros(e);
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            objeto.ValidarLetras(e);
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            objeto.ValidarLetras(e);
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            objeto.ValidarNumeros(e);
        }
    }
}
