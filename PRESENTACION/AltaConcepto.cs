﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NEGOCIO;

namespace PRESENTACION
{
    public partial class AltaConcepto : Form
    {
        public AltaConcepto()
        {
            InitializeComponent();
        }

        public Concepto concepto;
        public void cargarConceptos(Concepto concepto)
        {
            this.concepto = concepto;
        }

        private void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Concepto.Listar();
        }

        private void AltaConcepto_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedItem = null;
            comboBox1.SelectedItem = "Positivo";
            Enlazar();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text) && string.IsNullOrEmpty(textBox2.Text) && string.IsNullOrEmpty(textBox3.Text))
            {
                MessageBox.Show("Ingrese la informacion");
            }
            else
            {
                Concepto p = new Concepto();
                p.Nombre = textBox1.Text;
                p.Descripcion = textBox2.Text;
                p.Porcentaje = float.Parse(textBox3.Text);
                if (comboBox1.SelectedItem.ToString() == "Positivo")
                {
                    p.Operador = true;
                }
                else
                {
                    p.Operador = false;
                }

                p.Insertar();

                MessageBox.Show("Concepto dado de alta");
                textBox1.Clear();
                textBox2.Clear();
                textBox3.Clear();
                Enlazar();
            }
            
        }

        IValidar objeto = new VALIDACIONES();

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            objeto.ValidarLetras(e);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            objeto.ValidarLetras(e);
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            objeto.ValidarNumeros(e);
        }
    }
}
