﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NEGOCIO;

namespace PRESENTACION
{
    public partial class BajaConcepto : Form
    {
        public BajaConcepto()
        {
            InitializeComponent();
        }


        public Concepto concepto;
        public void cargarConceptos(Concepto concepto)
        {
            this.concepto = concepto;
        }

        private void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Concepto.Listar();

        }

        private void BajaConcepto_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            label6.Text = null;
            label7.Text = null;
            label8.Text = null;
            label9.Text = null;
            label10.Text = null;
            try
            {
                Concepto p = new Concepto(int.Parse(label6.Text));
                p.Borrar();

                Enlazar();
            }
            catch (Exception)
            {
                MessageBox.Show("Seleccione un empleado");
            }

            label6.Text = null;
            label7.Text = null;
            label8.Text = null;
            label9.Text = null;
            label10.Text = null;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            label6.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString(); 
            label7.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            label8.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();

            if (dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString() == "True")
            {
                label9.Text = "Positivo";
            }
            else
            {
                label9.Text = "Negativo";
            }

            label10.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString(); 
        }
    }
}
