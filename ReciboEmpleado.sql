USE [master]
GO
/****** Object:  Database [ParcialEmpleadoRecibo]    Script Date: 30/09/2020 12:41:57 p. m. ******/
CREATE DATABASE [ParcialEmpleadoRecibo]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ParcialEmpleadoRecibo', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\ParcialEmpleadoRecibo.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ParcialEmpleadoRecibo_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\ParcialEmpleadoRecibo_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ParcialEmpleadoRecibo].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET ARITHABORT OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET  MULTI_USER 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET QUERY_STORE = OFF
GO
USE [ParcialEmpleadoRecibo]
GO
/****** Object:  Table [dbo].[Concepto]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Concepto](
	[id_Concepto] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Descripcion] [varchar](max) NOT NULL,
	[Operador] [bit] NOT NULL,
	[Porcentaje] [float] NOT NULL,
 CONSTRAINT [PK_Concepto] PRIMARY KEY CLUSTERED 
(
	[id_Concepto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empleado](
	[Legajo] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido] [varchar](50) NOT NULL,
	[Cuil] [int] NOT NULL,
	[FechaAlta] [datetime] NOT NULL,
 CONSTRAINT [PK_Empleado] PRIMARY KEY CLUSTERED 
(
	[Legajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recibo]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recibo](
	[NroRecibo] [int] NOT NULL,
	[Mes] [varchar](50) NOT NULL,
	[Año] [varchar](50) NOT NULL,
	[SueldoBruto] [money] NOT NULL,
	[SueldoNeto] [money] NOT NULL,
	[TotalDescuentos] [money] NOT NULL,
 CONSTRAINT [PK_Recibo] PRIMARY KEY CLUSTERED 
(
	[NroRecibo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recibo_Concepto]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recibo_Concepto](
	[NroRecibo] [int] NOT NULL,
	[Id_Concepto] [int] NOT NULL,
 CONSTRAINT [PK_Recibo_Concepto] PRIMARY KEY CLUSTERED 
(
	[Id_Concepto] ASC,
	[NroRecibo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recibo_Legajo]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recibo_Legajo](
	[NroRecibo] [int] NOT NULL,
	[Legajo] [int] NOT NULL
) ON [PRIMARY]
GO
INSERT [dbo].[Concepto] ([id_Concepto], [Nombre], [Descripcion], [Operador], [Porcentaje]) VALUES (1, N'Jubilacion', N'jub', 0, 10)
GO
INSERT [dbo].[Empleado] ([Legajo], [Nombre], [Apellido], [Cuil], [FechaAlta]) VALUES (1, N'Gonzalo', N'Romero', 123, CAST(N'2020-09-30T00:00:00.000' AS DateTime))
INSERT [dbo].[Empleado] ([Legajo], [Nombre], [Apellido], [Cuil], [FechaAlta]) VALUES (2, N'Christian', N'Parkinson', 123, CAST(N'2020-09-30T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Recibo] ([NroRecibo], [Mes], [Año], [SueldoBruto], [SueldoNeto], [TotalDescuentos]) VALUES (1, N'9', N'2020', 10000.0000, 9000.0000, 1000.0000)
INSERT [dbo].[Recibo] ([NroRecibo], [Mes], [Año], [SueldoBruto], [SueldoNeto], [TotalDescuentos]) VALUES (2, N'9', N'2020', 15000.0000, 13500.0000, 1500.0000)
GO
INSERT [dbo].[Recibo_Concepto] ([NroRecibo], [Id_Concepto]) VALUES (1, 1)
INSERT [dbo].[Recibo_Concepto] ([NroRecibo], [Id_Concepto]) VALUES (2, 1)
GO
INSERT [dbo].[Recibo_Legajo] ([NroRecibo], [Legajo]) VALUES (2, 1)
INSERT [dbo].[Recibo_Legajo] ([NroRecibo], [Legajo]) VALUES (1, 1)
GO
/****** Object:  StoredProcedure [dbo].[CONCEPTO_BORRAR]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CONCEPTO_BORRAR]
@id_concept int
as
BEGIN

DELETE FROM CONCEPTO
where
id_Concepto = @id_concept

end
GO
/****** Object:  StoredProcedure [dbo].[CONCEPTO_EDITAR]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CONCEPTO_EDITAR]
@id_concept int, @nom varchar(50), @desc varchar(MAX), @oper bit, @val float
as
BEGIN

update Concepto set
nombre = @nom,
descripcion = @desc,
operador = @oper,
porcentaje = @val
where
id_Concepto = @id_concept

end
GO
/****** Object:  StoredProcedure [dbo].[CONCEPTO_INSERTAR]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[CONCEPTO_INSERTAR]
@nom varchar(50), @desc varchar(MAX), @oper bit, @val float
AS
BEGIN
declare @id_concept int

set @id_concept = isnull((SELECT MAX(ID_CONCEPTO) from CONCEPTO), 0) +1

insert Concepto values (@id_concept,@nom,@desc,@oper,@val)

end
GO
/****** Object:  StoredProcedure [dbo].[CONCEPTO_LISTAR]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[CONCEPTO_LISTAR]
as
BEGIN
SELECT * FROM CONCEPTO
END
GO
/****** Object:  StoredProcedure [dbo].[EMPLEADO_BORRAR]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[EMPLEADO_BORRAR]
@legajo int
as
BEGIN

DELETE FROM Empleado
where
legajo = @Legajo
END
GO
/****** Object:  StoredProcedure [dbo].[EMPLEADO_EDITAR]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[EMPLEADO_EDITAR]
@legajo int, @nom varchar(50), @apell varchar(50), @cuil int
as
BEGIN

update Empleado set
nombre = @nom,
apellido = @apell,
cuil = @cuil
where
legajo = @legajo

END

GO
/****** Object:  StoredProcedure [dbo].[EMPLEADO_INSERTAR]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[EMPLEADO_INSERTAR]
@nom varchar(50), @apell varchar(50), @cuil int, @fechaAlta datetime
as
BEGIN
declare @legajo int

set @legajo = isnull((SELECT MAX(LEGAJO) from EMPLEADO), 0) +1

insert Empleado values (@legajo,@nom,@apell,@cuil,@fechaAlta)

END

GO
/****** Object:  StoredProcedure [dbo].[EMPLEADO_LISTAR]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[EMPLEADO_LISTAR]
as
BEGIN
SELECT * FROM EMPLEADO
END

GO
/****** Object:  StoredProcedure [dbo].[RECIBO_CONCEPTO_INSERTAR]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[RECIBO_CONCEPTO_INSERTAR]
@idC int
AS
BEGIN
declare @nrorecibo int;

set @nrorecibo = (SELECT MAX(NRORECIBO) from RECIBO)

INSERT Recibo_Concepto VALUES (@nrorecibo,@idC)

END
GO
/****** Object:  StoredProcedure [dbo].[RECIBO_CONCEPTO_LISTAR]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[RECIBO_CONCEPTO_LISTAR]
as
BEGIN
SELECT * FROM Recibo_Concepto
END
GO
/****** Object:  StoredProcedure [dbo].[RECIBO_EMPLEADO_INSERTAR]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[RECIBO_EMPLEADO_INSERTAR]
@legajo int
AS
BEGIN
declare @nrorecibo int;

set @nrorecibo = (SELECT MAX(NRORECIBO) from RECIBO)

INSERT Recibo_Legajo VALUES (@nrorecibo,@legajo)

END
GO
/****** Object:  StoredProcedure [dbo].[RECIBO_INSERTAR]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[RECIBO_INSERTAR]
@mes varchar(50), @ano varchar(50), @sueldob money, @sueldon money, @totalDes money
as
BEGIN
declare @nrorecibo int

set @nrorecibo = isnull((SELECT MAX(NRORECIBO) from RECIBO), 0) +1

insert Recibo values (@nrorecibo,@mes,@ano,@sueldob,@sueldon,@totalDes)

END
GO
/****** Object:  StoredProcedure [dbo].[RECIBO_LEGAJO_LISTAR]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[RECIBO_LEGAJO_LISTAR]
as
BEGIN
SELECT * FROM RECIBO_LEGAJO
END
GO
/****** Object:  StoredProcedure [dbo].[RECIBO_LISTAR]    Script Date: 30/09/2020 12:41:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[RECIBO_LISTAR]
as
BEGIN
SELECT * FROM RECIBO
END
GO
USE [master]
GO
ALTER DATABASE [ParcialEmpleadoRecibo] SET  READ_WRITE 
GO
