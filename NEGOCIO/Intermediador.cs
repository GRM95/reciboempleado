﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO;
using System.Data;

namespace NEGOCIO
{
    public class Intermediador
    {
        public Intermediador() { }

        public Intermediador(int idC)
        {
            idConcepto = idC;
        }

        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }


        private int idConcepto;

        public int IdConcepto
        {
            get { return idConcepto; }
            set { idConcepto = value; }
        }



        private Acceso acceso = new Acceso();


        public void InsertarReciboConcepto()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@idC", this.idConcepto));
            
            acceso.Escribir("RECIBO_CONCEPTO_INSERTAR", parameters);

            acceso.Cerrar();
        }

    }
}
