﻿using System;
using System.Windows.Forms;

namespace NEGOCIO
{
    public interface IValidar
    {
        void ValidarLetras(KeyPressEventArgs e);
        bool ValidarFecha(DateTime fechaInicio);
        void ValidarNumeros(KeyPressEventArgs e);
    }
}