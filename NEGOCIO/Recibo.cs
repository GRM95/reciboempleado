﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO;
using System.Data;

namespace NEGOCIO
{
    public class Recibo
    {
        public Recibo() { }

        public Recibo(int r, int m, int a, float sb, float sn, float td)
        {
            nroRecibo = r;
            mes = m;
            ano = a;
            sueldoBruto = sb;
            sueldoNeto = sn;
            TotalDescuento = td;

            if (nroRecibo == 0)
            {
                Insertar();
            }
        }

        public Recibo(int l)
        {
            nroRecibo = l;
        }

        private int nroRecibo;

        public int NroRecibo
        {
            get { return nroRecibo; }
            set { nroRecibo = value; }
        }


        private int mes;

        public int Mes
        {
            get { return mes; }
            set { mes = value; }
        }

        private int ano;

        public int Ano
        {
            get { return ano; }
            set { ano = value; }
        }


        private float sueldoBruto;

        public float SueldoBruto
        {
            get { return sueldoBruto; }
            set { sueldoBruto = value; }
        }

        private float sueldoNeto;

        public float SueldoNeto
        {
            get { return sueldoNeto; }
            set { sueldoNeto = value; }
        }

        private float totalDescuento;

        public float TotalDescuento
        {
            get { return totalDescuento; }
            set { totalDescuento = value; }
        }


        private Acceso acceso = new Acceso();


        public void Insertar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@mes", this.mes));
            parameters.Add(acceso.CrearParametro("@ano", this.ano));
            parameters.Add(acceso.CrearParametro("@sueldob", this.sueldoBruto));
            parameters.Add(acceso.CrearParametro("@sueldon", this.sueldoNeto));
            parameters.Add(acceso.CrearParametro("@totalDes", this.TotalDescuento));


            acceso.Escribir("RECIBO_INSERTAR", parameters);

            acceso.Cerrar();
        }

        public void InsertarQuery(int id)
        {
            acceso.Abrir();
            acceso.Escribir("select * from empleado e, recibo r, concepto c, recibo_legajo rl, Recibo_Concepto rc where e.Legajo = rl.legajo and rl.NroRecibo = r.NroRecibo and r.NroRecibo = rc.NroRecibo and rc.Id_Concepto = c.id_Concepto and e.Legajo = " + id);
            acceso.Cerrar();

        }

        public static List<Recibo> Listar()
        {
            List<Recibo> lista = new List<Recibo>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("RECIBO_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Recibo p = new Recibo();
                p.nroRecibo = int.Parse(registro[0].ToString());
                p.mes = int.Parse(registro[1].ToString());
                p.ano = int.Parse(registro[2].ToString());
                p.sueldoBruto = float.Parse(registro[3].ToString());
                p.sueldoNeto = float.Parse(registro[4].ToString());
                p.totalDescuento = float.Parse(registro[5].ToString());



                lista.Add(p);
            }
            return lista;
        }

    }
}
