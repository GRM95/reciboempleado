﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO;
using System.Data;
using System.Security.Cryptography;

namespace NEGOCIO
{
    public class Empleado
    {
        public Empleado() { }

        public Empleado(int I, string n, string a, int c)
        {


            legajo = I;
            nombre = n;
            apellido = a;
            cuil = c;

            if (legajo == 0)
            {
                Insertar();
            }


        }

        public Empleado(int l)
        {
            legajo = l;
        }


        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private int cuil;

        public int Cuil
        {
            get { return cuil; }
            set { cuil = value; }
        }

        private DateTime fechaAlta;

        public DateTime FechaAlta
        {
            get { return fechaAlta; }
            set { fechaAlta = value; }
        }



        private Acceso acceso = new Acceso();

        public void InsertarReciboLegajo()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@legajo", this.legajo));

            acceso.Escribir("RECIBO_EMPLEADO_INSERTAR", parameters);

            acceso.Cerrar();
        }



        public void Insertar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@nom", this.nombre));
            parameters.Add(acceso.CrearParametro("@apell", this.apellido));
            parameters.Add(acceso.CrearParametro("@cuil", this.cuil));
            parameters.Add(acceso.CrearParametro("@fechaAlta", this.fechaAlta));



            acceso.Escribir("EMPLEADO_INSERTAR", parameters);

            acceso.Cerrar();
        }

        public void Editar()
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@nom", this.nombre));
            parameters.Add(acceso.CrearParametro("@legajo", this.legajo));
            parameters.Add(acceso.CrearParametro("@apell", this.apellido));
            parameters.Add(acceso.CrearParametro("@cuil", this.cuil));

            acceso.Escribir("EMPLEADO_EDITAR", parameters);

            acceso.Cerrar();
        }

        public void Borrar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@legajo", this.legajo));
            acceso.Escribir("EMPLEADO_BORRAR", parameters);

            acceso.Cerrar();
        }

        public static List<Empleado> Listar()
        {
            List<Empleado> lista = new List<Empleado>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("EMPLEADO_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Empleado p = new Empleado();
                p.legajo = int.Parse(registro[0].ToString());
                p.nombre = registro[1].ToString();
                p.apellido = registro[2].ToString();
                p.cuil = int.Parse(registro[3].ToString());
                p.fechaAlta = DateTime.Parse(registro[4].ToString());



                lista.Add(p);
            }
            return lista;
        }


    }
}
