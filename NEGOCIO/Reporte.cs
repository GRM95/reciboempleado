﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO;
using System.Data;
using System.Net.Http;

namespace NEGOCIO
{
    public class Reporte
    {
        public Reporte() { }

        public Reporte(int l, string n, string a, int c, DateTime f, int nr, int me, int an, float sueldob, float sueldon, float totaDesc, int idC, string nomConcepto, string descConcept, bool oper, int porcent)
        {
            legajo = l;
            nombre = n;
            apellido = a;
            cuil = c;
            fechaAlta = f;
            mes = me;
            ano = an;
            sueldoBruto = sueldob;
            sueldoNeto = sueldon;
            totalDescuento = totaDesc;
            idConcepto = idC;
            nombreConcepto = nomConcepto;
            descripcionConcepto = descConcept;
            operador = oper;
            porcentaje = porcent;

        }


        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private int cuil;

        public int Cuil
        {
            get { return cuil; }
            set { cuil = value; }
        }

        private DateTime fechaAlta;

        public DateTime FechaAlta
        {
            get { return fechaAlta; }
            set { fechaAlta = value; }
        }

        private int nroRecibo;

        public int NroRecibo
        {
            get { return nroRecibo; }
            set { nroRecibo = value; }
        }

        private int mes;

        public int Mes
        {
            get { return mes; }
            set { mes = value; }
        }

        private int ano;

        public int Ano
        {
            get { return ano; }
            set { ano = value; }
        }

        private float sueldoBruto;

        public float SueldoBruto
        {
            get { return sueldoBruto; }
            set { sueldoBruto = value; }
        }

        private float sueldoNeto;

        public float SueldoNeto
        {
            get { return sueldoNeto; }
            set { sueldoNeto = value; }
        }

        private float totalDescuento;

        public float TotalDescuento
        {
            get { return totalDescuento; }
            set { totalDescuento = value; }
        }

        private int idConcepto;

        public int IdConcepto
        {
            get { return idConcepto; }
            set { idConcepto = value; }
        }

        private string nombreConcepto;

        public string NombreConcepto
        {
            get { return nombreConcepto; }
            set { nombreConcepto = value; }
        }

        private string descripcionConcepto;

        public string DescripcionConcepto
        {
            get { return descripcionConcepto; }
            set { descripcionConcepto = value; }
        }

        private bool operador;

        public bool Operador
        {
            get { return operador; }
            set { operador = value; }
        }

        private int porcentaje;

        public int Porcentaje
        {
            get { return porcentaje; }
            set { porcentaje = value; }
        }



        public static List<Reporte> Listar(int id)
        {
            List<Reporte> lista = new List<Reporte>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Reporte("select * from empleado e, recibo r, concepto c, recibo_legajo rl, Recibo_Concepto rc where e.Legajo = rl.legajo and rl.NroRecibo = r.NroRecibo and r.NroRecibo = rc.NroRecibo and rc.Id_Concepto = c.id_Concepto and e.Legajo =" + id);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Reporte p = new Reporte();
                p.legajo = int.Parse(registro[0].ToString());
                p.nombre = registro[1].ToString();
                p.apellido = registro[2].ToString();
                p.cuil = int.Parse(registro[3].ToString());
                p.fechaAlta = DateTime.Parse(registro[4].ToString());
                p.nroRecibo = int.Parse(registro[5].ToString());
                p.mes = int.Parse(registro[6].ToString());
                p.ano = int.Parse(registro[7].ToString());
                p.sueldoBruto = float.Parse(registro[8].ToString());
                p.sueldoNeto = float.Parse(registro[9].ToString());
                p.totalDescuento = float.Parse(registro[10].ToString());
                p.idConcepto = int.Parse(registro[11].ToString());
                p.nombreConcepto = registro[12].ToString();
                p.descripcionConcepto = registro[13].ToString();
                p.operador = bool.Parse(registro[14].ToString());
                p.porcentaje = int.Parse(registro[15].ToString());


                lista.Add(p);
            }
            return lista;
        }










    }
}
