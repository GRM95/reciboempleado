﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACCESO;
using System.Data;

namespace NEGOCIO
{
    public class Concepto
    {
        public Concepto() { }

        public Concepto(int I, string n, string d, bool o, float p)
        {


            idConcepto = I;
            nombre = n;
            descripcion = d;
            operador = o;
            porcentaje = p;

            if (idConcepto == 0)
            {
                Insertar();
            }


        }

        public Concepto(int l)
        {
            idConcepto = l;
        }


        private int idConcepto;

        public int IdConcepto
        {
            get { return idConcepto; }
            set { idConcepto = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string descripcion;

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        private bool operador;

        public bool Operador
        {
            get { return operador; }
            set { operador = value; }
        }

        private float porcentaje;

        public float Porcentaje
        {
            get { return porcentaje; }
            set { porcentaje = value; }
        }



        private Acceso acceso = new Acceso();

        public void InsertarId()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@nom", this.nombre));

            acceso.Escribir("", parameters);

            acceso.Cerrar();
        }

        public void Insertar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@nom", this.nombre));
            parameters.Add(acceso.CrearParametro("@desc", this.descripcion));
            parameters.Add(acceso.CrearParametro("@oper", this.operador));
            parameters.Add(acceso.CrearParametro("@val", this.porcentaje));



            acceso.Escribir("CONCEPTO_INSERTAR", parameters);

            acceso.Cerrar();
        }

        public void Editar()
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@nom", this.nombre));
            parameters.Add(acceso.CrearParametro("@id_concept", this.idConcepto));
            parameters.Add(acceso.CrearParametro("@desc", this.descripcion));
            parameters.Add(acceso.CrearParametro("@oper", this.operador));
            parameters.Add(acceso.CrearParametro("@val", this.porcentaje));


            acceso.Escribir("CONCEPTO_EDITAR", parameters);

            acceso.Cerrar();
        }

        public void Borrar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@id_concept", this.idConcepto));
            acceso.Escribir("CONCEPTO_BORRAR", parameters);

            acceso.Cerrar();
        }

        public static List<Concepto> Listar()
        {
            List<Concepto> lista = new List<Concepto>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("CONCEPTO_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Concepto p = new Concepto();
                p.idConcepto = int.Parse(registro[0].ToString());
                p.nombre = registro[1].ToString();
                p.descripcion = registro[2].ToString();
                p.operador = bool.Parse(registro[3].ToString());
                p.porcentaje = float.Parse(registro[4].ToString());

                lista.Add(p);
            }
            return lista;
        }
    }
}
